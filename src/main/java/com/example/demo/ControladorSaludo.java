package com.example.demo;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/api/v1/saludos")
public class ControladorSaludo {

    static class Saludo{
        long id;
        //Atributos publicos spring los serializa(deserializa
        public String saludoCorto;
        public String saludoLargo;
    }
    private final AtomicLong secuenciaIds = new AtomicLong(0L);
    private final List<Saludo> saludos = new LinkedList<Saludo>();
    @GetMapping("/")
    public List<Saludo> obtenerTodosLosSaludos(){
        return this.saludos;
    }

    @PostMapping("/")
    public Saludo crearSaludo(@RequestBody Saludo saludo){
        saludo.id=this.secuenciaIds.incrementAndGet();
       this.saludos.add(saludo);
        return saludo;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarSaludo(@PathVariable long id){
        for(int i=0;i<this.saludos.size();i++){
            Saludo saludo = this.saludos.get(i);
            if(id == saludo.id ){
                this.saludos.remove(i);
                return;
            }
        }
    }

    @GetMapping("/{id}")
    public Saludo devolverUnSaludo(@PathVariable long id){
        for(Saludo saludo : this.saludos){
            if(id == saludo.id ){
                return saludo;
            }
        }
        throw  new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}")
    //@ResponseStatus(HttpStatus.ACCEPTED)
    public void reemplazarSaludo(@PathVariable long id,@RequestBody Saludo saludoNuevo){
        for(int i=0;i<this.saludos.size();i++){
            Saludo saludo = this.saludos.get(i);
            if(id == saludo.id ){
                saludo.saludoCorto=saludoNuevo.saludoCorto;
                saludo.saludoLargo=saludoNuevo.saludoLargo;
                this.saludos.set(i,saludoNuevo);
                return ;
            }
        }
        throw  new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    static class ParcheSaludo{
        public String operacion;
        public String atributo;
        public Saludo saludo;
    }

    @PatchMapping("/{id}")
    //@ResponseStatus(HttpStatus.ACCEPTED)
    public void modificarSaludo(@PathVariable long id,@RequestBody ParcheSaludo parche){
        for(int i=0;i<this.saludos.size();i++){
            Saludo saludo = this.saludos.get(i);
            if(id == saludo.id ){
                this.saludos.set(i,emparcharSaludo(saludo,parche));
                return ;
            }
        }
        throw  new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    private Saludo emparcharSaludo(Saludo saludo,ParcheSaludo parche){
        if(parche.operacion.trim().equalsIgnoreCase("borrar-atributo")){
            if("saludoCorto".equalsIgnoreCase(parche.atributo)){
                saludo.saludoCorto=null;
            }
            if("saludoLargo".equalsIgnoreCase(parche.atributo)){
                saludo.saludoLargo=null;
            }
        }else if(parche.operacion.trim().equalsIgnoreCase("modificar-atributos")){
            if(parche.saludo.saludoCorto!=null){
                saludo.saludoCorto=parche.saludo.saludoCorto;
            }
            if(parche.saludo.saludoLargo!=null){
                saludo.saludoLargo=parche.saludo.saludoLargo;
            }

        }
        return saludo;
    }
}
